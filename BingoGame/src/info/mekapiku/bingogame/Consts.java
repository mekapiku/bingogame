package info.mekapiku.bingogame;

public final class Consts {
	// アプリケーション名
    public static final String APP_NAME = "接待ビンゴ"; 
	// ビンゴカードの最大数
    public static final int BINGO_MAX = 75;
    // 八百長の度合い(1が最高)
    public static final int RAND_RANGE = 1;
    // フォントサイズ
    public static final int FONT_SIZE = 600;
    // 読み込みファイル(実行ファイルと同一フォルダに置いてね！！)
    public static final String FILE_NAME = "Bingo.txt";
    // フレームレート
	public static final long FRAME_RATE = (long)(1000.0f/60.f+0.5f);
}
